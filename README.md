> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# Mobile web app develpoment LIS 4381

## Dakota Van Aulen

### LIS 4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Course instalations: Android Studios, Java, and AMPSS
    - Create Bitbucket repo
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - Provide git command description 

2. [A2 README.md](a2/README.md "My A2 README.md file")

    - Creating Healthy Recipes Android App 
    - Provide Screenshots of completed app 
    - Complete Assigned skillset numbers 1-2 

3. [A3 Read.md](a3/README.md "My A3 README.md file")

    - Create mobile web application called "My Event"
    - Create and connect database for mobile web application
    - Screen shots of skillset projects 4-6
    - Three tables of ten database records

4. [A4 Read.md](a4/README.md "My A4 README.md file")
    - Develop a web based data entry form.
    - Use jQuery client based data validation to avoid SQL injection.
    - Finish skillsets 10-12.

5. [p1 README.md](p1/README.md "My p1 README.md file")
    
    - Create Business card Android app
    - Provide screenshot of completed app
    - Complete assigned skillsets 7-9  

6. [a5 README.md](a5/README.md "My a5 README.md file")

    - Develop a web based data entry form that displays the sql data table.
    - Use jQuery client based data validation to avoid SQL injection as well as server side data validation.
    - Finish skillsets 13-15.
    