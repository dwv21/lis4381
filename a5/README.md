# LIS 4381

 

## Dakota Van Aulen

 

> ### Assignment #5 Requirements:

 

1. Develop a web based data entry form that displays the sql data table.

2. Use jQuery client based data validation to avoid SQL injection as well as server side data validation.

3. Finish skillsets 13-15.

 

> #### README.md file should include the following items:

 

* Screenshot of A5 index.

* Screenshot of invalid data with error message.

* Screenshot of valid data with new record.

* Screenshots of Skillsets.

 

> #### Assignment Screenshots:

 

### A5 Index

![A5 Index](img/1st_index.png)

 

### Valid Data Entry

Valid Data                                  |New Record                              

--------------------------------------------|------------------------------------------------

![Valid Data ](img/add_pet_store(passedd_validation).png)          |![New Record](img/add_pet_store_(valid).png)            

 

### Invalid Data Entry

Invalid Data                                  |Error Message                              

----------------------------------------------|------------------------------------------------

![Invalid Data ](img/Add_pet_store_php(invalid).png)          |![Error Message](img/add_pet_store_process(failed_validation).png)  

 

> #### *Assignment Skillsets*:

 

Simple Calculator PHP             |Read/Write Data PHP

----------------------------------|-----------------------------

![skillset 13](img/ss13_Sphere_volume_calculator_pic.png)  | ![skillset 14](img/ss14_simple_caclulator(process_functions.php2).png)
![skillset 14](img/ss14_simple_caculator(process_functions.php).png)  | ![skillset 14](img/ss14_simple_calculator(index.php).png)
![skillset 14](img/ss14_simple_calculator(index.php2).png)


 

### Volume of a Sphere Calculator

![skillset 15](img/ss15_write_read(index.php1).png)  | ![skillset 15](img/ss15_write_read_file_two.png) 

 