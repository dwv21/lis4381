import java.util.*;
import java.util.Scanner; //import the scanner documentation

import javax.print.DocPrintJob;

import java.lang.*;//import the math documentation
import java.text.*;

public class Methods {
    // create global scanner object sc so it can be used by multiple methods of the
    // Methods class
    // "final" prevents object variable from being modified
    static final Scanner sc = new Scanner(System.in);

    public static void getRequirements() {

        System.out.println("Made by Dakota Van Aulen\n");

        System.out.println("Sphere Volume Calculator \n");

        System.out.println("Program calculates sphere volume in liquid U.S gallons from user-entered diameter value in inches and rounds to two decimal places.");

        System.out.println("Use Java built in PI and pow() capabilities");

        System.out.println("Program continues to prompt for user entry until no longer requrested.");

        System.out.println("Note: Propmt also accepts lower case letters. \n");

    }

    public static void calVol() {
        // initializes variables and scanner object
        int diameter = 0;
        double volume = 0.0;
        double gallons = 0.0;
        char choice = ' ';
        Scanner sc = new Scanner(System.in); //initalizes scanner object 

        //do while loop for prompting the user over and over until they don't want to do it anymore and make them do the calculation at least once
        do 
        {
            System.out.print("Please enter diameter in inches: ");
            while (!sc.hasNextInt())
            {
                System.out.println("Not valid integer!\n");
                sc.next(); //if omitted this part, the loop will become infinite loop, as the user never gets to enter their data
                System.out.print("Please try again. Enter diameter in inches: ");
            }//checks user input to see if it is an integer
            diameter = sc.nextInt();//assign the user entered data into the variable diameter

            System.out.println(); //print a blank line

            //use floating point values to not get integer division
            volume = ((4.0/3.0) * Math.PI * Math.pow(diameter/2.0,3));//returns cube inches
            gallons = volume/231;//converts cube inches into gallons
            System.out.println("Sphere volume: " + String.format("%,.2f", gallons) + " liquid U.S gallons");

            System.out.print("\nDo you want to calculate another sphere volume (y or n)?");
            choice = sc.next().charAt(0);//gets the first character of the user input
            choice = Character.toLowerCase(choice);//converts the user's input into lowercase for comparison
        }
        while (choice == 'y');

        System.out.println("/n Thank you for using our Sphere Volume Calculator!");
    }

}
// end of the class
