import java.util.*;
import java.util.Scanner; //import the scanner documentation
import java.lang.*;//import the math documentation

public class Methods {

    public static void getRequirements() {

        System.out.println("Made by Dakota Van Aulen");

        System.out.println("This generates numbers in a specific range that the user provided");

        System.out.println("Implement random number generator in while, do_while, for, enhanced_for loops");
    }

    public static void doRan() {
        // create scanner object and initialize variables bound1 and bound2
        Scanner scnr = new Scanner(System.in);
        int bound1;
        int bound2;
        int numbersNeed;

        // prompt user for bounds and put them in variable bound1 and bound2
        System.out.print("Integer.Min_Value: ");
        bound1 = scnr.nextInt();

        System.out.print("Integer.Max_Value: ");
        bound2 = scnr.nextInt();

        // prompt user for the amount of numbers they need to get
        System.out.print("Enter how many numbers do you need: ");
        numbersNeed = scnr.nextInt();

        // start loop to loop through the function as many times as the user wants
        int times = 0;
        System.out.println("while loop");
        while (times < numbersNeed) {
            System.out.println(randInt(bound1, bound2));
            times++;
        }

        System.out.println();
        System.out.println("for loop");
        for (int t = 0; t < numbersNeed; t++) {
            System.out.println(randInt(bound1, bound2));
        }

        System.out.println();
        System.out.println("enhanced for loop");
        int[] intArray = new int[numbersNeed];
        for (int m = 0; m < numbersNeed; m++) {
            intArray[m] = randInt(bound1, bound2);
        }
        for (int f : intArray) {
            System.out.println(f);
        }

        System.out.println();
        System.out.println("do while loop");
        int b = 0;
        do {
            System.out.println(randInt(bound1, bound2));
            b++;
        } while (b < numbersNeed);
    }

    public static int randInt(int bound1, int bound2) {

        // create a random object
        Random ran = new Random();

        // using the new random object to generate random numbers between bonuds
        int fin = ran.nextInt(bound1, bound2);
        return fin;
    }

}
// end of the class