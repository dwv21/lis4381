import java.util.*;
import java.util.Scanner; //import the scanner documentation
import java.lang.*;//import the math documentation

public class Methods {

    public static void getRequirements() {

        System.out.println("Made by Dakota Van Aulen");

        System.out.println("Program prompts user for first name and age, then prints results");

        System.out.println("Create four methods from the following requirements:");

        System.out.println("1) getRequirements(): Void method displays program requirements.");

        System.out.println("2) getUserInput(): Void method displays program requirements.");

        System.out.println("     then calls two methods: myVoidMethod() and myValue");

        System.out.println("3) myVoidMethod():");

        System.out.println("      a. Accepts two arguments: String and int.");

        System.out.println("      b. Prints user's first name and age");

        System.out.println("4) myValueReturningMethod(): ");

        System.out.println("      a. Accepts two arguments: String and int.");

        System.out.println("      b. Returns String containing first naem and age");

        System.out.println();

    }

    public static void getUserInput() {
        // create scanner object scnr
        Scanner scnr = new Scanner(System.in);

        // initialize variables
        int age = 0;
        String name = "";

        // prompt user for first and age and stores them in variables;

        System.out.print("Enter your first name: ");
        name = scnr.nextLine();

        System.out.print("Enter age: ");
        age = scnr.nextInt();

        // pass values stored in name and age to myVoidMethod() and
        // myValueReturningMethod()

        myVoidMethod(name, age);

        System.out.println(myValueReturningMethod(name, age));

    }

    public static void myVoidMethod(String name, int age) {
        // print the name and age of the user in one sentence
        System.out.println("void method call: " + name + " is " + age);

    }

    public static String myValueReturningMethod(String name, int age) {

        // store the sentence I want to return in string

        String sentence = "value returning method call: ";

        // return the string
        return (sentence + " " + name + " is " + age);
    }

}
// end of the class