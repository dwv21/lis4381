import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Methods.getRequirements();

        char character;

        System.out.print("Enter a character: ");

        Scanner scnr = new Scanner(System.in);

        character = scnr.next().charAt(0);

        System.out.println("\nIf Else way");
        Methods.ifElse(character);

        System.out.println("\nSwitch method");
        Methods.switchWay(character);
    }
}