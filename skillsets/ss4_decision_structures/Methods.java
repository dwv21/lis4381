import java.util.*;
import java.util.Scanner; //import the scanner documentation
import java.lang.*;//import the math documentation

public class Methods {

    public static void getRequirements() {

        System.out.println("Made by Dakota Van Aulen");

        System.out.println("Use following characters: W or w, C or c, H or h, N or n");

        System.out.println("Use following decision structures, if_else and switch");
    }

    public static void ifElse(char ch) {
        // convert to lowercase
        ch = Character.toLowerCase(ch);

        // initialize char variables and use them for comparisons;
        char ch1, ch2, ch3, ch4;

        ch1 = 'w';
        ch2 = 'c';
        ch3 = 'h';
        ch4 = 'n';

        if (ch == ch1) {
            System.out.println("Phone type: work");
        } else if (ch == ch2) {
            System.out.println("Phone type: cell");
        } else if (ch == ch3) {
            System.out.println("Phone type: home");
        } else if (ch == ch4) {
            System.out.println("Phone type:none");
        } else {
            System.out.println("Wrong phone");
        }

    }// end of ifElse

    public static void switchWay(char ch) {
        // convert to lowercase
        ch = Character.toLowerCase(ch);

        // initialize char variables and use them for comparisons;
        char ch1, ch2, ch3, ch4;

        ch1 = 'w';
        ch2 = 'c';
        ch3 = 'h';
        ch4 = 'n';

        switch (ch) {
            case 'w':
                System.out.println("Phone type: Work");
                break;

            case 'c':
                System.out.println("Phone type: Cell");
                break;

            case 'h':
                System.out.println("Phone type: Home");
                break;

            case 'n':
                System.out.println("Phone type: None");
                break;

            default:
                System.out.println("Unknown input");
                break;
        }
    }

}
// end of the class