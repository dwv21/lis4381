> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 Mobile Web Applications

## Dakota Van Aulen

### Assignment 2 Requirements:

*three parts:*

1. Made mobile app using Android Studios
2. Development programs using things Java according to the requirments of skill sets problems
3. Ran mobile app using emulator

## README.md file should include the following items:

* Course title, your name, assignment requirements, as per A2;
* Screenshot of skillset 1 - Even or Odd
* Screenshot of skillset 2 - Largest Number
* Screenshot of skillset 3 - Arrays and Loops
* Screenshot of running application's first user interface;
* Screenshot of running application's second user interface;
* This is a blockquote.

This is the second paragraph in the blockquote.

### Assignments Screenshots


*Screenshot of main screen of finished mobile app *:

![Screenshot of main screen of finished mobile app](img/Bruschetta_Recipepic.png "Screen one screenshot")

*Screenshot of app screen 2*:

![Screenshot of finished mobile app page 2](img/Bruschettapic2.png "Screenshot two")

*Screenshot of Screenshot of Skillset 1 Even or Odd*:

![Screenshot of Screenshot of Skillset 1 Even or Odd](img/skillset1pic.png "screenshot skillset 1 Even or Odd")

*Screenshot of Screenshot of Skillset 2 Largest Number*:

![Screenshot of Screenshot of Skillset 2 Largest Number](img/skillset2pic.png "screenshot skillset 2 Largest Number")

*Screenshot of Screenshot of Skillset 3 Arrays and Loops*:

![Screenshot of Screenshot of Skillset 3 Arrays and Loops](img/skillset3.png "screenshot skillset 3")






