> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 Mobile Web Application Development

## Dakota Van Auen

### Assignment 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Android Studio and Java first app

>
> #### Git commands w/short descriptions:

1. git init - initialize the repo locally
2. git status - describes the state of the current working directory and the staging area
3. git add - tells git to include update in the next commit
4. git commit - create a new commit with the current contents and giving a message describing the changes
5. git push - upload files from local to remote repo
6. git pull - download files from remote to local repo
7. got clone - target a repo and make a copy of that repo
#### Assignment Screenshots:

*Screenshot of running php*:

![JDK Installation Screenshot](img/php.png)


*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of running java*:

![JDK Installation Screenshot](img/java.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/phone_pic.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
