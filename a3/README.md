> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 Mobile Web Applications

## Dakota Van Aulen

### Assignment 3 Requirements:

*three parts:*

1. create mobile web application called "My Event"
2. Create and connect database for mobile web application
3. Screen shots of skillset projects 4-6
4. Three tables of ten database records

## README.md file should include the following items:

* Screenshot of ERD
* Screenshot of running application opening user interface
* Screenshot of running application processing user input 
* Screenshot of 10 records for each table
* Screenshot of skill sets 4-6
* Links to the following files: a3.mwb and a3.sql 

### Assignments Screenshots

 ![Screenshot of ERD](img/sqlcharts.png "Screenshot of Entity Relationship Diagram") 
 
 ![Screenshot of Records from Petstore table](img/petstoretable.png "Screenshot of 10 records From Petstore Table") 



![Screenshot of records from pet table](img/pettable.png "Screenshot of 10 records from pet table") 

![Screenshot of records from customer table](img/customertable.png "Screenshot of 10 records from customer table") 



![My Event Opening Interface Screenshot](img/mainss.png "Screenshot of my Main Event Application Opening Interface")

![My Event - First Processing Interface](mainss.png "First Screenshot Interface for My Event") 

![My Event - Second Processing Interface](img/firstprocessingapp.png "Second Screenshot Interface For My Event")  



 ![Forth Skillset Screenshot - Decision Structures](img/ss4.png "Screenshot of my forth skillset code running") 
 
 ![Fifth Skillset Screenshot - Random Number Generator](img/ss5.png "Screenshot of my fifth skillset code running") 
 
 ![Sixth Skillset Screenshot - Methods](img/ss6.png "Screenshot of my sixth skillset code running") 


 ### Document Links


 [a3.mwb](docs/a3.mwb "Link to Assignment 3 mySQL Workbench database Document")



 *Links to a3.sql*:
 [a3.sql](docs/a3.sql "Link to Assignment 3 MySQL document")

 




