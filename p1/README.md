README.md

> > **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Dakota Van Aulen

### Project 1 Requirements:

*Sub-Heading:*

1. Android Studio - My Business Card App
2. Completed Skillsets 7-9
3. Chapter Questions (Chs. 7,8)

#### README.md file should include the following items:

* Screenshot of first user interface for My Business Card App
* Screenshot of second user interface for My Business Card App
* Screenshots of skillsets 7-9
* Chs 7,8 Completed

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.



#### Assignment Screenshots:



| *Screenshot of Android Studio - My Business Card App opening first user interface*: | *Screenshot of Android Studio - Concert App processing user input*: |
| ---------- | ----------|
| ![My Business Card App - Opening First interface](img/project1.png "Screenshot of opening first interface") | ![My Business Card App - Second interface](img/project12.png "Screenshot of second user interface") |


*Screenshot of Skillset 7 - Random Number Generator Data Validation*:

![Skillset 7 - Random Number Generator Data Validation](img/ss7.png "Skillset 7 Screenshot")

*Screenshot of Skillset 8 - Largest Three Numbers*:

![Skillset 8 - Largest Three Numbers](img/ss8.png "Skillset 8 Screenshot")

*Screenshot of Skillset 9 - Array Runtime Data Validation*:

![Skillset 9 - Array Runtime Data Validation](img/ss9.png "Skillset 9 Screenshot")